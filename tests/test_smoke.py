"""
Smoke tests
Purpose: verify the correct component initialization
"""

from common import mypackage

def test_smoke():
    assert mypackage.VERSION
    assert mypackage.main() == 0

