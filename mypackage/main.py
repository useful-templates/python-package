#! usr/bin/env python3
"""
Application entry point
"""

def main() -> int:
    print("Hello, World!")
    return 0

if __name__ == "__main__":
    from sys import exit
    exit(main())
